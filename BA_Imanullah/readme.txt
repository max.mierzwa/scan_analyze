scripts to analyze laserscan data

1. preprocessing
	import from csv
	removing baseplate
	scaling of x-axis
	result: layerXX.parquet

2. refine
	isolates weld surface using machine learning
	result: weldXX.parquet

3. analyze
	3.1 analyze_width
		returns data, plot or something else
	3.2 analyze_height
