import pandas as pd
import matplotlib.pyplot as plt

from multiprocessing import Pool
from glob import glob

from sklearn.preprocessing import StandardScaler
from sklearn.cluster import DBSCAN

def get_DBSCAN_mask(group_df):

    # Extract 'Y' and 'Z' columns as input for DBSCAN
    features = group_df[['Y', 'Z']]

    # normalize data
    scaler = StandardScaler().fit(features)
    features = scaler.transform(features)
    
    # Apply DBSCAN
    dbscan = DBSCAN(eps=0.2, min_samples=2,n_jobs=1)
    labels = dbscan.fit_predict(features)

    # Replace -1 with 4 (sorting doesn't like -1)
    labels[labels == -1] = max(labels)+1

    # Add DBSCAN labels to the DataFrame
    group_df['labels'] = labels

    # Calculate average z-value for each label and sort accordingly
    label_avg_z = group_df.groupby('labels')['Z'].median()
    sorted_labels = label_avg_z.sort_values().index

    # use sorting to rename labels
    group_df['sorted_labels'] = group_df['labels'].map({label: i for i, label in 
                                                        enumerate(sorted_labels)})

    # get weld surface points for mask
    sorted_labels_list = group_df['sorted_labels'].tolist()
    mask = [val == max(sorted_labels_list) for val in sorted_labels_list]

    return mask