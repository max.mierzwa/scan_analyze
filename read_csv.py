import pandas as pd

# function to read a CSV file and return a dataframe
def read_csv(filename):
    df = pd.read_csv(filename, header=None)
    df = df[0].str.split(';', expand=True)
    df = df.drop(columns=df.columns[-1], axis=1)
    return df