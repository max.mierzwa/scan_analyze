scripts to analyze laserscan data

1. slicing
	import from csv
	scaling of x-axis
	isolates weld surface using density based clustering
	result: layerXX.parquet

2. analyze
	2.1 analyze_width
		get local layer width
		make plot
	2.2 analyze_height
		calculate local layer height
		make plot
	2.3 analyze_vol
		calculate local volume of layers

3. using parameter space
	Example: There's a wide wall (10 mm). I want to add thin layers on top. 
	How can this goal be achieved?
	3.1 setup look-up table: use desired width or height to find welding parameters
